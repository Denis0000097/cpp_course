# Курс по C++ 

## Общая информация

Курс читается в ФПМИ МФТИ для потока русскоговорящих иностранцев. 

ВАЖНО: Текущий main - поток 2022-2023

Автор вдохновлялся [замечательным курсом](https://www.youtube.com/channel/UCGlYKd-FR4g0Tp4wF6_wxig) Ильи Мещерина 

## Навигация

- [Предварительная программа курса](/program/program_in_dev.md)
- [Конспекты лекций](/lectures)
- [Планы семинаров](/sems)

## Контакты автора 

- [tg](https://t.me/yaishenka)
- [vk](https://vk.com/ya1shenka)
- [github](https://github.com/yaishenka)
